# Copyright 2021 Ivan Yelizariev <https://twitter.com/yelizariev>
# License MIT (https://opensource.org/licenses/MIT).

from flectra import http
from flectra.http import request

from flectra.addons.mail.controllers.main import MailController


class MultiLivechatMailController(MailController):
    @http.route()
    def mail_init_messaging(self):
        values = super().mail_init_messaging()
        values["multi_livechat"] = request.env["mail.channel"].multi_livechat_info()
        return values
